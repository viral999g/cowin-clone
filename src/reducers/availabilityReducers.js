import {
  AVAILABILITY_LIST_SUCCESS,
  AVAILABILITY_LIST_FAIL,
  UPDATE_FILTERS,
  RESET_FILTERS,
  SET_CURRENT_DATE,
  RESET_SEARCH
} from '../constants/availabilityConstants';

export const availabilityListReducer = (state = { centers: [], filters: {}, filteredCenters: [], startDate: '', currentDate: '', searchStatus: false, searchType: '', searchParams: {} }, action) => {
  switch (action.type) {
    case SET_CURRENT_DATE:
      return { ...state, currentDate: action.payload.currentDate, error: null, centers: action.payload.centers, filteredCenters: action.payload.filteredCenters, filters: action.payload.filters };
    case AVAILABILITY_LIST_SUCCESS:
      return { ...state, error: null, centers: action.payload.centers, filteredCenters: action.payload.filteredCenters, filters: action.payload.filters, searchStatus: true, searchType: action.payload.searchType, searchParams: action.payload.searchParams };
    case AVAILABILITY_LIST_FAIL:
      return { ...state, searchStatus: true, error: action.payload };
    case UPDATE_FILTERS:
      return { ...state, filters: action.payload.filters, filteredCenters: action.payload.filteredCenters };
    case RESET_FILTERS:
      return { ...state, filters: action.payload.filters, filteredCenters: action.payload.filteredCenters };
    case RESET_SEARCH:
      return { ...state, searchStatus: false }
    default:
      return state;
  }
};
