import { PUBLIC_REPORTS_SUCCESS } from "../constants/publicReportsConstants";

export const publicReportReducer = (state = { report: {} }, action) => {
  switch (action.type) {
    case PUBLIC_REPORTS_SUCCESS:
      return { ...state, report: action.payload };
    default:
      return state;
  }
};