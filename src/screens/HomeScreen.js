import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { TelephoneFill } from 'react-bootstrap-icons';
import HomePageCarousel from '../components/HomePageCarousel';
import HelplineItem from '../components/HelplineItem';
import SearchSection from '../components/SearchSection';
import AvailabilityResults from '../components/availabilityResults/AvailabilityResults';
import GetVaccinatedSection from '../components/GetVaccinatedSection';
import ContactUsSection from '../components/ContactUsSection';
import FAQSection from '../components/FAQSection';
import VaccinationDrive from '../components/VaccinationDrive';
import Footer from '../components/Footer';
import Header from '../components/Header'
import MainHeader from '../components/MainHeader'

const HomeScreen = () => {
  return (
    <>
      <Header></Header>
      <MainHeader></MainHeader>
      <HomePageCarousel></HomePageCarousel>
      <section id='helpline' className='my-4'>
        <Container className='horizontal-scrollable'>
          <Row>
            <Col
              xs={12}
              sm={12}
              lg={2}
              id='helplineHeader'
              className='text-uppercase align-middle text-center'>
              <h5 className='border-end border-1s border-primary py-1 my-0 fw-normal fs-6'>
                <TelephoneFill /> <span className='ps-2'>helpline</span>
              </h5>
            </Col>

            <Col
              xs={12}
              sm={12}
              lg={10}
              className='helpline__desc mt-3 text-center text-lg-start mt-lg-0 mx-lg-0'>
              <Row className='pb-2 mx-lg-1'>
                <HelplineItem
                  itemTitle='Number'
                  numValue='911123978046'
                  numText='91-11-23978046'></HelplineItem>

                <HelplineItem
                  itemTitle='Health Ministry'
                  numValue='1075'></HelplineItem>

                <HelplineItem itemTitle='Child' numValue='1098'></HelplineItem>

                <HelplineItem
                  itemTitle='Mental Health'
                  numValue='08046110007'></HelplineItem>

                <HelplineItem
                  itemTitle='Senior Citizens'
                  numValue='14567'></HelplineItem>
              </Row>
            </Col>
          </Row>
        </Container>
      </section>

      <SearchSection></SearchSection>
      <AvailabilityResults></AvailabilityResults>
      <GetVaccinatedSection></GetVaccinatedSection>
      <ContactUsSection></ContactUsSection>
      <FAQSection></FAQSection>
      <VaccinationDrive></VaccinationDrive>
      <Footer></Footer>
    </>
  );
};

export default HomeScreen;
