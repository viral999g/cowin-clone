import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import Header from '../components/Header'
import MainHeader from '../components/MainHeader'
import RegistrationFooter from '../components/RegistrationFooter'
import RegistrationForm from '../components/RegistrationForm'
import { CONFIRM_OTP_RESET, SEND_OTP_RESET } from '../constants/userConstants'


const RegistrationScreen = ({ history }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    return () => {
      dispatch({ type: SEND_OTP_RESET })
      dispatch({ type: CONFIRM_OTP_RESET })
    }
  })

  return (
    <div className='d-flex flex-column' id='registration__container'>
      <div className='registration__header'>
        <Header></Header>
        <MainHeader></MainHeader>
      </div>
      <div className='registration_content'>
        <div className="register-wrap">
          <MDBRow className='d-flex justify-content-center'>
            <MDBCol xs={12} lg={4}>
              <MDBRow>
                <RegistrationForm history={history}></RegistrationForm>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </div>
      </div>
      <RegistrationFooter></RegistrationFooter>
    </div>
  )
}

export default RegistrationScreen
