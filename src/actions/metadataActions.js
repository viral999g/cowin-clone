import axios from 'axios';
import { COWIN_BASE_URL } from '../constants/configs';
import { DISTRICT_LIST_SUCCESS, ID_TYPES_SUCCESS, STATE_LIST_SUCCESS } from '../constants/metadataConstants';

export const getStatesList = (date) => async (dispatch) => {
  try {
    const { data } = await axios(`${COWIN_BASE_URL}/v2/admin/location/states`)
    dispatch({
      type: STATE_LIST_SUCCESS,
      payload: data.states
    })

  } catch (error) {
    console.log(error)
  }
}

export const getDistrictsList = (state_id) => async (dispatch) => {
  try {
    const { data } = await axios(`${COWIN_BASE_URL}/v2/admin/location/districts/${state_id}`)
    dispatch({
      type: DISTRICT_LIST_SUCCESS,
      payload: data.districts
    })

  } catch (error) {
    console.log(error)
  }
}


export const getIdTypes = () => async (dispatch) => {
  try {
    const { data } = await axios('/api/v2/registration/beneficiary/idTypes')
    dispatch({
      type: ID_TYPES_SUCCESS,
      payload: data.types
    })

  } catch (error) {
    console.log(error)
  }
}