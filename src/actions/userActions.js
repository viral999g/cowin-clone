import axios from 'axios';
import { sha256 } from 'js-sha256';
import { COWIN_SANDBOX_HEADERS } from '../constants/configs';
import { BENEFICIARIES_DATA_FAIL, BENEFICIARIES_DATA_REQUEST, BENEFICIARIES_DATA_SUCCESS, CONFIRM_OTP_FAIL, CONFIRM_OTP_REQUEST, CONFIRM_OTP_SUCCESS, SEND_OTP_FAIL, SEND_OTP_REQUEST, SEND_OTP_SUCCESS } from '../constants/userConstants';


export const sendOTP = (number) => async (dispatch) => {
  try {
    dispatch({
      type: SEND_OTP_REQUEST,
      payload: number
    })

    const { data } = await axios.post(`/api/v2/auth/generateOTP`, JSON.stringify({ mobile: number }), COWIN_SANDBOX_HEADERS)

    if ('txnId' in data) {
      dispatch({
        type: SEND_OTP_SUCCESS,
        payload: data.txnId
      })
    } else {
      throw new Error()
    }

  } catch (error) {
    console.log(error)
    dispatch({
      type: SEND_OTP_FAIL,
      payload: 'Something went wrong!'
    })
  }
}

export const confirmOTP = (otp) => async (dispatch, getState) => {
  try {
    const {
      userSendOTP: { number, txnId },
    } = getState();

    dispatch({
      type: CONFIRM_OTP_REQUEST,
      payload: number
    })

    const { data } = await axios.post(`/api/v2/auth/confirmOTP`, JSON.stringify({ txnId, otp: sha256(otp) }), COWIN_SANDBOX_HEADERS)

    if ('token' in data) {
      dispatch({
        type: CONFIRM_OTP_SUCCESS,
        payload: data.token
      })
    } else {
      dispatch({
        type: CONFIRM_OTP_FAIL,
        payload: 'Something went wrong!'
      })
    }

  } catch (error) {
    if (error.response.status === 400) {
      dispatch({
        type: CONFIRM_OTP_FAIL,
        payload: 'Invalid OTP!'
      })
    } else if (error.response.status === 401) {
      dispatch({
        type: CONFIRM_OTP_FAIL,
        payload: 'Something went wrong!'
      })
    }
  }
}

export const getBeneficiaries = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: BENEFICIARIES_DATA_REQUEST,
    })

    const {
      userConfirmOTP: { token },
    } = getState();

    const { data } = await axios.get(`/api/v2/appointment/beneficiaries`, { headers: { ...COWIN_SANDBOX_HEADERS.headers, 'Authorization': `Bearer ${token}` } })

    if ('beneficiaries' in data) {
      // dispatch({
      //   type: BENEFICIARIES_DATA_SUCCESS,
      //   payload: data.beneficiaries
      // })
    } else {
      throw new Error()
    }

  } catch (error) {
    console.log(error)
    dispatch({
      type: BENEFICIARIES_DATA_FAIL,
      payload: 'Something went wrong!'
    })
  }
}