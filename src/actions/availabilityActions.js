import axios from 'axios';

import { AVAILABILITY_LIST_SUCCESS, AVAILABILITY_LIST_FAIL, SET_CURRENT_DATE } from '../constants/availabilityConstants';
// import { UPDATE_FILTERS } from '../constants/filterConstants'
import { COWIN_BASE_URL } from '../constants/configs';

const initial_filters = {
  'age': {
    '18-plus': {
      selected: false,
      label: '18+'
    },
    '18-44': {
      selected: false,
      label: '18-44'
    },
    '45-plus': {
      selected: false,
      label: '45+'
    }
  }, 'cost': {
    'paid': {
      selected: false,
      label: 'paid'
    },
    'free': {
      selected: false,
      label: 'free'
    }
  }, 'vaccine': {
    'covishield': {
      selected: false,
      label: 'COVISHIELD'
    },
    'covaxin': {
      selected: false,
      label: 'COVAXIN'
    },
    'sputnik': {
      selected: false,
      label: 'SPUTNIK V'
    }
  }
}

const getData = async (searchType, searchParams, date) => {
  let url = null
  if (searchType === 'pincode') {
    url = `${COWIN_BASE_URL}/v2/appointment/sessions/public/calendarByPin?pincode=${searchParams.pincode}&date=${date}`
  } else if (searchType === 'district') {
    url = `${COWIN_BASE_URL}/v2/appointment/sessions/public/calendarByDistrict?district_id=${searchParams.district_id}&date=${date}`
  }

  const { data } = await axios.get(url);
  return data

}

export const getAvailabilityList = (date) => async (dispatch, getState) => {
  try {
    const {
      availabilityList: { searchType, searchParams }
    } = getState();

    const data = await getData(searchType, searchParams, date)

    dispatch({
      type: SET_CURRENT_DATE,
      payload: { currentDate: date, centers: data.centers, filteredCenters: data.centers, filters: { ...initial_filters } },
    })


  } catch (error) {
    dispatch({
      type: AVAILABILITY_LIST_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
}

export const initiateSearch = (type = '', params = {}, ini = false) => async (dispatch, getState) => {
  try {
    const {
      availabilityList: { currentDate: date, searchType, searchParams, filters }
    } = getState();

    if (ini && searchType === '') {
      return false
    }

    if (ini) {
      params = { ...searchParams }
      type = searchType
    }

    const data = await getData(type, params, date)

    dispatch({
      type: AVAILABILITY_LIST_SUCCESS,
      payload: { searchType: type, searchParams: params, centers: data.centers, filteredCenters: data.centers, filters: ini ? { ...filters } : { ...initial_filters } },
    })
  } catch (error) {
    dispatch({
      type: AVAILABILITY_LIST_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }

}