import React, { useEffect } from 'react'

const SearchByMap = () => {
  useEffect(() => {
    window.loadMap(document.getElementById('nearby-covid-places-mapmyindia'))
  }, [])
  return (
    <div>
      <div id="nearby-covid-places-mapmyindia"></div>
    </div>
  )
}

export default SearchByMap
