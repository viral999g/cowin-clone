import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {
  MDBTypography,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane
} from 'mdb-react-ui-kit';
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';
import SearchByPin from './SearchByPin';
import SearchByDistrict from './SearchByDistrict';
import SearchByMap from './SearchByMap'
import { RESET_SEARCH } from '../constants/availabilityConstants';
import { initiateSearch } from '../actions/availabilityActions';

const SearchSection = () => {
  const availabilityList = useSelector(state => state.availabilityList);
  const { searchType, searchParams } = availabilityList;

  const [basicActive, setBasicActive] = useState(searchType !== '' ? searchType : 'pincode');
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(initiateSearch(undefined, undefined, true))
  }, [dispatch])

  const handleBasicClick = (value) => {
    if (value === basicActive) {
      return;
    }

    if (value === 'map') {
      dispatch({ type: RESET_SEARCH })
    }

    setBasicActive(value);
  };

  return (
    <MDBContainer className='mt-5 mt-md-5 text-primary' id='searchSection' as='section' breakpoint='xl'>
      <MDBTypography tag='h1' className='fw-normal font-family-roboto text-capitalise text-center px-1'>Check Your Nearest Vaccination Center And Slots Availability</MDBTypography>

      <MDBRow className='d-flex justify-content-center mt-3 mt-md-5'>
        <MDBCol lg={6} md={9} sm={12} className='justify-content-center'>
          <MDBTabs className='mb-3'>
            <MDBTabsItem>
              <MDBTabsLink onClick={() => handleBasicClick('pincode')} active={basicActive === 'pincode'}>
                Search by PIN
              </MDBTabsLink>
            </MDBTabsItem >
            <MDBTabsItem>
              <MDBTabsLink onClick={() => handleBasicClick('district')} active={basicActive === 'district'}>
                Search by District
              </MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink onClick={() => handleBasicClick('map')} active={basicActive === 'map'}>
                Search by Map
              </MDBTabsLink>
            </MDBTabsItem>
          </MDBTabs>
        </MDBCol>

        <MDBTabsContent>
          <MDBTabsPane show={basicActive === 'pincode'}>
            <SearchByPin searchParams={searchParams}></SearchByPin>
          </MDBTabsPane>
          <MDBTabsPane show={basicActive === 'district'}>
            <SearchByDistrict searchParams={searchParams}></SearchByDistrict>
          </MDBTabsPane>
          <MDBTabsPane show={basicActive === 'map'}><SearchByMap></SearchByMap> </MDBTabsPane>
        </MDBTabsContent>
      </MDBRow>
    </MDBContainer>
  )
}

export default SearchSection
