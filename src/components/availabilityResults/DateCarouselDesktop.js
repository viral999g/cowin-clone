import React, { useRef, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { MDBRow, MDBCol, MDBContainer } from 'mdb-react-ui-kit'
import { ChevronLeft, ChevronRight } from 'react-bootstrap-icons'
import Slider from "react-slick";
import moment from 'moment';
import { getAvailabilityList } from '../../actions/availabilityActions';
// import { Carousel, ListGroup } from 'react-bootstrap';
// import Carousel from 'react-multi-carousel'
// import "react-multi-carousel/lib/styles.css";

const NextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <ChevronRight width={20} height={20} className={className} onClick={onClick} color='#000' style={{ ...style, display: "block" }}></ChevronRight>
  );
}

const PrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <ChevronLeft width={20} height={20} className={className} onClick={onClick} color='#000' style={{ ...style, display: "block" }}></ChevronLeft>
  )
}


const DateCarouselDesktop = ({ columns }) => {
  const slider1 = useRef();
  const dispatch = useDispatch()

  const availabilityList = useSelector(state => state.availabilityList);
  const { startDate } = availabilityList
  const endDate = moment(startDate, "DD-MM-YYYY")
  endDate.add(11, 'w')

  const dateChangeHandler = useCallback((next) => {
    var newDate = moment(startDate, "DD-MM-YYYY").add(next, 'days').format("DD-MM-YYYY")
    dispatch(getAvailabilityList(newDate))
  }, [startDate, dispatch])

  console.log("Date Carousel Desktop")
  const dates = []
  for (var m = moment(startDate, "DD-MM-YYYY"); m.isBefore(endDate); m.add(1, 'days')) {
    dates.push(<div className='slide-item' key={m.millisecond}>
      <p>
        {m.format("D MMM")}
        <span> {m.get('year')}</span>
      </p>
    </div>)
  }

  const settings = {
    dots: false,
    infinite: false,
    fade: false,
    speed: 0,
    slidesToShow: columns,
    slidesToScroll: columns,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    beforeChange: (current, next) => { dateChangeHandler(next) }
  };

  return (
    <MDBContainer breakpoint='xl'>
      <MDBRow>
        <MDBCol md={3}></MDBCol>
        <MDBCol md={9}>
          <Slider {...settings} ref={slider => (slider1.current = slider)}>
            {dates}
          </Slider>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default DateCarouselDesktop
