import { MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBContainer } from 'mdb-react-ui-kit'
import React from 'react'
import { Col, Row } from 'react-bootstrap'

import img1 from '../assets/images/vaccination-thumb-1.jpg'
import img2 from '../assets/images/vaccination-thumb-2.jpg'
import img3 from '../assets/images/vaccination-thumb-3.jpg'

const GetVaccinatedSection = () => {
  return (
    <MDBContainer id='getVaccinatedSection' className='mt-5 py-5' breakpoint='xl'>
      <h1 className='text-center mb-5'>How To Get Vaccination</h1>
      <Row className='justify-content-center'>
        <Col sm={6} md={4} xs={12} className='mt-0 mt-sm-4'>
          <MDBCard>
            <MDBCardImage className="img-fluid rounded-circle img-card" src={img1} />
            <MDBCardBody>
              <MDBCardText>
                Book An Appointment On <br /> CoWIN or Walk Into Any <br /> Vaccination Center
              </MDBCardText>
              <p><a target='_blank' rel="noreferrer" href="https://prod-cdn.preprod.co-vin.in/assets/pdf/User_Guide_Citizen%20registration_18%2B.pdf">HOW TO BOOK YOUR APPOINTMENT ON COWIN</a></p>
            </MDBCardBody>
          </MDBCard>
        </Col>

        <Col sm={6} md={4} xs={12} className='mt-0 mt-sm-4'>
          <MDBCard>
            <MDBCardImage className="img-fluid rounded-circle img-card" src={img2} />
            <MDBCardBody>
              <MDBCardText>
                Get Your Vaccination <br /> Safely at the Time of Your <br /> Appointment
              </MDBCardText>
              <p><a target='_blank' rel="noreferrer" href="https://prod-cdn.preprod.co-vin.in/assets/pdf/Dos_and_Donts_for_Citizens.pdf">DOS AND DONT'S FOR GETTING VACCINATED</a></p>
            </MDBCardBody>
          </MDBCard>
        </Col>

        <Col sm={6} md={4} xs={12} className='mt-0 mt-sm-4'>
          <MDBCard>
            <MDBCardImage className="img-fluid rounded-circle img-card" src={img3} />
            <MDBCardBody>
              <MDBCardText>
                Download Your Vaccination <br />Certificate from CoWIN <br /> and Wait for Dose #2
              </MDBCardText>
              <p><a target='_blank' rel="noreferrer" href="https://selfregistration.cowin.gov.in/vaccination-certificate">DOWNLOAD YOUR VACCINE CERTIFICATE</a></p>
            </MDBCardBody>
          </MDBCard>
        </Col>
      </Row>
    </MDBContainer>
  )
}

export default GetVaccinatedSection
