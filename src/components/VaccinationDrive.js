import { MDBContainer } from 'mdb-react-ui-kit'
import React, { useEffect } from 'react'
import { Col, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getPublicReports } from '../actions/publicReportActions';
import { PeopleFill, BoxArrowUpRight } from 'react-bootstrap-icons';

const numberToText = (num) => {
  var t = num
  var text = `${num}`

  if (t / 1000 > 1) {
    text = `${(t / 1000).toPrecision(4)} Thousands`
  }
  if (t / 100000 > 1) {
    text = `${(t / 100000).toPrecision(4)} Lakhs`
  }
  if (t / 10000000.0 > 1) {
    text = `${(t / 10000000).toPrecision(4)} Crores`
  }

  return text
}

const VaccinationDrive = () => {
  const dispatch = useDispatch()

  let topBlock = {};

  const { report } = useSelector(state => state.publicReport)
  if ('topBlock' in report) {
    topBlock = report.topBlock
  }



  useEffect(() => {
    dispatch(getPublicReports())
  }, [dispatch]);

  return 'topBlock' in report && (
    <MDBContainer breakpoint='xl' id='vaccinationDriveSection' className='my-5'>
      <h1 className='text-center mb-5'>Join The Largest Vaccination Drive</h1>
      <ul className="mt-5">
        <li>
          <div className='card'>
            <Row>
              <Col xs={3}>
                <PeopleFill width={32} height={32}></PeopleFill>
              </Col>
              <Col xs={9}>
                <h6>Cowin Registrations</h6>
                <h4>{numberToText(topBlock.registration.total)}</h4>
                <h5><span>+ {numberToText(topBlock.registration.yesterday)}</span> Yesterday</h5>
              </Col>
            </Row>
          </div>
        </li>

        <li>
          <div className='card'>
            <Row>
              <Col xs={3}>
                <PeopleFill width={32} height={32}></PeopleFill>
              </Col>
              <Col xs={9}>
                <h6>Vaccinations Delivered</h6>
                <h4>{numberToText(topBlock.vaccination.total)}</h4>
                <h5><span>+ {numberToText(topBlock.vaccination.yesterday_vac)}</span> Yesterday</h5>
              </Col>
            </Row>
          </div>
        </li>

        <li>
          <div className='card'>
            <Row>
              <Col xs={3}>
                <PeopleFill width={32} height={32}></PeopleFill>
              </Col>
              <Col xs={9}>
                <h6>Fully Vaccinated</h6>
                <h4>{numberToText(topBlock.vaccination.tot_dose_2)}</h4>
                <h5><span>+ {numberToText(topBlock.vaccination.yesterday_dose_two)}</span> Yesterday</h5>
              </Col>
            </Row>
          </div>
        </li>

      </ul>

      <ul className="d-flex justify-content-center">
        <a href="https://dashboard.cowin.gov.in/" className="btn btn-secondary btn-sm btn-view-dashboard" target='_blank' rel='noreferrer'>View Vaccination Dashboard
          <BoxArrowUpRight width={8} height={8} />
        </a>
      </ul>
    </MDBContainer>
  )
}

export default VaccinationDrive
