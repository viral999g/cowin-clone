import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import moment from 'moment';

import { availabilityListReducer } from './reducers/availabilityReducers';
import { metadataReducer } from './reducers/metadataReducers';
import { publicReportReducer } from './reducers/publicReportsReducers';
import { userBeneficiariesReducer, userConfirmOTPReducer, userSendOTPReducer } from './reducers/userReducers';

const reducer = combineReducers({
  availabilityList: availabilityListReducer,
  metadata: metadataReducer,
  publicReport: publicReportReducer,
  userSendOTP: userSendOTPReducer,
  userConfirmOTP: userConfirmOTPReducer,
  userBeneficiaries: userBeneficiariesReducer
})

const searchType = localStorage.getItem('searchType')
  ? localStorage.getItem('searchType')
  : '';

const searchParams = localStorage.getItem('searchParams')
  ? JSON.parse(localStorage.getItem('searchParams'))
  : { };

const initialState = {
  availabilityList: {
    searchStatus: false,
    startDate: moment().format('DD-MM-YYYY'),
    currentDate: moment().format('DD-MM-YYYY'),
    searchType,
    searchParams,
    centers: [],
    filters: {
      'age': {
        '18-plus': {
          selected: false,
          label: '18+'
        },
        '18-44': {
          selected: false,
          label: '18-44'
        },
        '45-plus': {
          selected: false,
          label: '45+'
        }
      }, 'cost': {
        'paid': {
          selected: false,
          label: 'paid'
        },
        'free': {
          selected: false,
          label: 'free'
        }
      }, 'vaccine': {
        'covishield': {
          selected: false,
          label: 'COVISHIELD'
        },
        'covaxin': {
          selected: false,
          label: 'COVAXIN'
        },
        'sputnik': {
          selected: false,
          label: 'SPUTNIK V'
        }
      }
    },
    filteredCenters: []
  },
  metadata: {
    states: [],
    districts: [],
    preferenceStatus: false
  },
  userConfirmOTP: {
    number: '7069852821'
  },
  userBeneficiaries:
  {
    beneficiaries: [{ "beneficiary_reference_id": "64559392067620", "name": "Viral A Gandhi", "birth_year": "1996", "gender": "Male", "mobile_number": "2821", "photo_id_type": "Driving License", "photo_id_number": "XXXXXXXXXXX0321", "comorbidity_ind": "N", "vaccination_status": "Vaccinated", "vaccine": "COVAXIN", "dose1_date": "20-05-2021", "dose2_date": "22-06-2021", "appointments": [{ "appointment_id": "9ae2a0c8-17eb-4954-aa68-db3e870ab3be", "center_id": 733544, "name": "Covaxin-2nd Dose School No.52", "state_name": "Gujarat", "district_name": "Rajkot Corporation", "block_name": "Central Zone", "from": "09:00", "to": "13:00", "dose": 2, "session_id": "e3fd41be-6fa8-4d90-8035-04bcbac35418", "date": "22-06-2021", "slot": "10:00AM-11:00AM" }, { "appointment_id": "7d7b5678-cb52-4d55-9b7b-0f495ac61831", "center_id": 439909, "name": "SARDAR SCHOOL SANT KABIR ROAD", "state_name": "Gujarat", "district_name": "Rajkot Corporation", "block_name": "East Zone", "from": "09:00", "to": "13:00", "dose": 1, "session_id": "f1f6ca41-5946-4922-9ef2-999fac75dbdd", "date": "20-05-2021", "slot": "12:00PM-01:00PM" }] }, { "beneficiary_reference_id": "24606932154840", "name": "Priyansh Ashok Gandhi", "birth_year": "2001", "gender": "Male", "mobile_number": "2821", "photo_id_type": 1, "photo_id_number": "XXXXXXXX2792", "comorbidity_ind": "N", "vaccination_status": "Vaccinated", "vaccine": "COVAXIN", "dose1_date": "14-05-2021", "dose2_date": "18-06-2021", "appointments": [{ "appointment_id": "c6b82ec6-ed24-4761-b523-dd4aee154806", "center_id": 584368, "name": "WEST ZONE OFFICE RMC 2", "state_name": "Gujarat", "district_name": "Rajkot Corporation", "block_name": "West Zone", "from": "09:30", "to": "13:00", "dose": 2, "session_id": "b0502b38-7938-4372-8c99-ab2eae00d183", "date": "18-06-2021", "slot": "11:30AM-01:00PM" }, { "appointment_id": "7dd2e846-2199-4598-8626-042ef5b3b0cd", "center_id": 692671, "name": "SCHOOL NO. 49/B SAMRAT N.R", "state_name": "Gujarat", "district_name": "Rajkot Corporation", "block_name": "Central Zone", "from": "09:00", "to": "18:00", "dose": 1, "session_id": "003fe10d-d330-490f-b0a8-19ec4e0d0fd3", "date": "14-05-2021", "slot": "03:00PM-06:00PM" }] }, { "beneficiary_reference_id": "85798958825410", "name": "Ashok Gandhi", "birth_year": "1996", "gender": "Male", "mobile_number": "2821", "photo_id_type": "Aadhaar Card", "photo_id_number": "XXXXXXXX6125", "comorbidity_ind": "N", "vaccination_status": "Not Vaccinated", "vaccine": "", "dose1_date": "", "dose2_date": "", "appointments": [] }],
    success: true,
    error: ''
  }

};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;